-----
title:  取代MybatisPlus？阿里推出了新 ORM 框架！ 
categories: ORM
tags: ["FluentMybatis"] 
date: 2023-07-27
-----
点击“IT码徒”，关注，置顶公众号

每日技术干货，第一时间送达！

  


> 耗时8个月联合打造 《[* *2023年Java高薪课程 ][2023_Java]》，**已****更新了 ****102G ****视频**，**累计更新时长 ****500+ ****个小时**，需要的小伙伴可以了解下，**一次购买，持续更新，无需2次付费。**

  


使用fluent mybatis可以不用写具体的xml文件，通过java api可以构造出比较复杂的业务sql语句，做到代码逻辑和sql逻辑的合一。  
不再需要在Dao中组装查询或更新操作，在xml或mapper中再组装参数。  
那对比原生Mybatis, Mybatis Plus或者其他框架，FluentMybatis提供了哪些便利呢？

  


**1**

**需求场景设置**

  


我们通过一个比较典型的业务需求来具体实现和对比下，假如有学生成绩表结构如下:  


    create table student_score (     id           bigint auto_increment comment '主键ID' primary key,     student_id bigint            not null comment '学号',     gender_man tinyint default 0 not null comment '性别, 0:女; 1:男',     school_term int               null comment '学期',     subject varchar(30) null comment '学科',     score int               null comment '成绩',     gmt_create datetime not null comment '记录创建时间',     gmt_modified datetime not null comment '记录最后修改时间',     is_deleted tinyint default 0 not null comment '逻辑删除标识' ) engine = InnoDB default charset=utf8;

  
现在有需求:  


> 统计2000年三门学科('英语', '数学', '语文')及格分数按学期,学科统计最低分，最高分和平均分, 且样本数需要大于1条,统计结果按学期和学科排序

  
我们可以写SQL语句如下  


    select school_term,        subject,        count(score) as count,        min(score) as min_score,        max(score) as max_score,        avg(score) as max_score from student_score where school_term >= 2000   and subject in ('英语', '数学', '语文')   and score >= 60   and is_deleted = 0 group by school_term, subject having count(score) > 1 order by school_term, subject;

  
那上面的需求，分别用fluent mybatis, 原生mybatis 和 Mybatis plus来实现一番。

  


**2**

**三者实现对比**

  


**使用fluent mybatis 来实现上面的功能**  
![968efcc4f904fd5c32783f95600c829a.png][] 

> 具体代码： https://gitee.com/fluent-mybatis/fluent-mybatis- docs/tree/master/spring-boot-demo/

  
我们可以看到fluent api的能力，以及IDE对代码的渲染效果。

  


**3**

**换成mybatis原生实现效果**

  


1、 定义Mapper接口；  


    public interface MyStudentScoreMapper {     List<Map<String, Object>> summaryScore(SummaryQuery paras); }

  
2、 定义接口需要用到的参数实体SummaryQuery；  


    @Data @Accessors(chain = true) public class SummaryQuery {     private Integer schoolTerm;     private List<String> subjects;     private Integer score;     private Integer minCount; }

  
3、 定义实现业务逻辑的mapperxml文件；  


    <select id="summaryScore" resultType="map" parameterType="cn.org.fluent.mybatis.springboot.demo.mapper.SummaryQuery">     select school_term,     subject,     count(score) as count,     min(score) as min_score,     max(score) as max_score,     avg(score) as max_score     from student_score     where school_term >= #{schoolTerm}     and subject in     <foreach collection="subjects" item="item" open="(" close=")" separator=",">         #{item}     </foreach>     and score >= #{score}     and is_deleted = 0     group by school_term, subject     having count(score) > #{minCount}     order by school_term, subject </select>

  
4、 实现业务接口(这里是测试类,实际应用中应该对应Dao类)；  


    @RunWith(SpringRunner.class) @SpringBootTest(classes = QuickStartApplication.class) public class MybatisDemo {     @Autowired     private MyStudentScoreMapper mapper;     @Test     public void mybatis_demo() {         // 构造查询参数         SummaryQuery paras = new SummaryQuery()             .setSchoolTerm(2000)             .setSubjects(Arrays.asList("英语", "数学", "语文"))             .setScore(60)             .setMinCount(1);         List<Map<String, Object>> summary = mapper.summaryScore(paras);         System.out.println(summary);     } }

  
总之，直接使用mybatis，实现步骤还是相当的繁琐，效率太低。那换成mybatis plus的效果怎样呢？

   


**4**

**换成mybatis plus实现效果**

  
mybatis plus的实现比mybatis会简单比较多，实现效果如下  
![862a919e7e35180d3895e05fa5b15576.png][]  
如红框圈出的，写mybatis plus实现用到了比较多字符串的硬编码（可以用Entity的get lambda方法部分代替字符串编码）。  
字符串的硬编码，会给开发同学造成不小的使用门槛，个人觉的主要有2点：  


 *  字段名称的记忆和敲码困难
 *  Entity属性跟随数据库字段发生变更后的运行时错误

  
其他框架，比如TkMybatis在封装和易用性上比mybatis plus要弱，就不再比较了。  
**生成代码编码比较**  
fluent mybatis生成代码设置  


    public class AppEntityGenerator {     static final String url = "jdbc:mysql://localhost:3306/fluent_mybatis_demo?useSSL=false&useUnicode=true&characterEncoding=utf-8";     public static void main(String[] args) {         FileGenerator.build(Abc.class);     }     @Tables(         /** 数据库连接信息 **/         url = url, username = "root", password = "password",         /** Entity类parent package路径 **/         basePack = "cn.org.fluent.mybatis.springboot.demo",         /** Entity代码源目录 **/         srcDir = "spring-boot-demo/src/main/java",         /** Dao代码源目录 **/         daoDir = "spring-boot-demo/src/main/java",         /** 如果表定义记录创建，记录修改，逻辑删除字段 **/         gmtCreated = "gmt_create", gmtModified = "gmt_modified", logicDeleted = "is_deleted",         /** 需要生成文件的表 ( 表名称:对应的Entity名称 ) **/         tables = @Table(value = {"student_score"})     )     static class Abc {     } }

  
mybatis plus代码生成设置  


    public class CodeGenerator {     static String dbUrl = "jdbc:mysql://localhost:3306/fluent_mybatis_demo?useSSL=false&useUnicode=true&characterEncoding=utf-8";     @Test     public void generateCode() {         GlobalConfig config = new GlobalConfig();         DataSourceConfig dataSourceConfig = new DataSourceConfig();         dataSourceConfig.setDbType(DbType.MYSQL)             .setUrl(dbUrl)             .setUsername("root")             .setPassword("password")             .setDriverName(Driver.class.getName());         StrategyConfig strategyConfig = new StrategyConfig();         strategyConfig             .setCapitalMode(true)             .setEntityLombokModel(false)             .setNaming(NamingStrategy.underline_to_camel)             .setColumnNaming(NamingStrategy.underline_to_camel)             .setEntityTableFieldAnnotationEnable(true)             .setFieldPrefix(new String[]{"test_"})             .setInclude(new String[]{"student_score"})             .setLogicDeleteFieldName("is_deleted")             .setTableFillList(Arrays.asList(                 new TableFill("gmt_create", FieldFill.INSERT),                 new TableFill("gmt_modified", FieldFill.INSERT_UPDATE)));         config             .setActiveRecord(false)             .setIdType(IdType.AUTO)             .setOutputDir(System.getProperty("user.dir") + "/src/main/java/")             .setFileOverride(true);         new AutoGenerator().setGlobalConfig(config)             .setDataSource(dataSourceConfig)             .setStrategy(strategyConfig)             .setPackageInfo(                 new PackageConfig()                     .setParent("com.mp.demo")                     .setController("controller")                     .setEntity("entity")             ).execute();     } }

  


**5**

**FluentMybatis特性一览**

  


![c8379f25f20dfd38cfa4a42408b902f4.png][]

  


**6**

**三者对比总结**

  


看完3个框架对同一个功能点的实现, 各位看官肯定会有自己的判断，笔者这里也总结了一份比较。  


![a3ba969848c155d273fbeb8f3b1a4e3e.png][]

  
来源：juejin.cn/post/6886019929519177735  
— END —

  


# [【福利】2023 高薪课程，全面来袭（视频+笔记+源码）][2023_Java] #

  
**PS：防止找不到本篇文章，可以收藏点赞，方便翻阅查找哦。**  


  


  


往期推荐

  
  
[RBAC权限模型，就该这么设计！][RBAC][去除烦人的 NullPointerException 空指针异常，这样做才够优雅！][NullPointerException][MySQL 巨坑：永远不要在 MySQL 中使用 UTF-8！！][MySQL _ MySQL _ UTF-8][为什么 MySQL 不建议使用 NULL 作为列默认值？][MySQL _ NULL][再见Jenkins！这款自动化部署工具更强大，还贼带劲！][Jenkins][不要再封装各种 Util 工具类了，这个神级框架值得拥有！][Util][图解 Redis，还有谁看不懂？][Redis]

  



[2023_Java]: http://mp.weixin.qq.com/s?__biz=MzU2OTMyMTAxNA==&mid=2247516711&idx=1&sn=e54d98dc55dd5dd2ff2c90935a60ae97&chksm=fc82b17ecbf53868aece859626913a43ba5b592398dcda1847e497df47632b180fb1fbd3a3d3&scene=21#wechat_redirect
[968efcc4f904fd5c32783f95600c829a.png]: https://hanqing90.gitee.io/image-hosting/2023/07/27/968efcc4f904fd5c32783f95600c829a.png
[862a919e7e35180d3895e05fa5b15576.png]: https://hanqing90.gitee.io/image-hosting/2023/07/27/862a919e7e35180d3895e05fa5b15576.png
[c8379f25f20dfd38cfa4a42408b902f4.png]: https://hanqing90.gitee.io/image-hosting/2023/07/27/c8379f25f20dfd38cfa4a42408b902f4.png
[a3ba969848c155d273fbeb8f3b1a4e3e.png]: https://hanqing90.gitee.io/image-hosting/2023/07/27/a3ba969848c155d273fbeb8f3b1a4e3e.png
[RBAC]: http://mp.weixin.qq.com/s?__biz=MzI3MDM0MzAyMg==&mid=2247499534&idx=2&sn=20e30a3657316ee1093446c536fd5785&chksm=ead0310fdda7b819e2b1864cc12adf3e9719854b39aa1d6d8aedcd68172920511a470edece64&scene=21#wechat_redirect
[NullPointerException]: http://mp.weixin.qq.com/s?__biz=MzI3MDM0MzAyMg==&mid=2247499508&idx=1&sn=227c9287680d6613466b0e5894a8e226&chksm=ead030f5dda7b9e3652d9dd92c2a415dabe5ee0a88dadaecb09e2052719c12d8997f7618536c&scene=21#wechat_redirect
[MySQL _ MySQL _ UTF-8]: http://mp.weixin.qq.com/s?__biz=MzI3MDM0MzAyMg==&mid=2247499496&idx=2&sn=ce21036e7a5903020bd6e46c6b2e7e41&chksm=ead030e9dda7b9ffd8f59045ab9fa8e54b081a314f788db99845a27988a830fd036d41325c8a&scene=21#wechat_redirect
[MySQL _ NULL]: http://mp.weixin.qq.com/s?__biz=MzI3MDM0MzAyMg==&mid=2247499493&idx=1&sn=0cb4f84d2da3c04cde90e8bbd4912de7&chksm=ead030e4dda7b9f2cc75b5f20a96d8111c91068ab73f43ad92431286954324337e0fa4b183a9&scene=21#wechat_redirect
[Jenkins]: http://mp.weixin.qq.com/s?__biz=MzI3MDM0MzAyMg==&mid=2247499482&idx=1&sn=143f8edad48283e5cabac4b497110905&chksm=ead030dbdda7b9cd232fcd5afb0131081e868bfccc435fd6787fb28d7b52e136596f4ee850eb&scene=21#wechat_redirect
[Util]: http://mp.weixin.qq.com/s?__biz=MzI3MDM0MzAyMg==&mid=2247499466&idx=1&sn=ada006aed64c6ce627a8c1f4fdb9127b&chksm=ead030cbdda7b9dd5b12604fdfe8032e55dc265c5f95ba22b72e44b89d127af612e826446c23&scene=21#wechat_redirect
[Redis]: http://mp.weixin.qq.com/s?__biz=MzI3MDM0MzAyMg==&mid=2247499461&idx=2&sn=47b5c48a041164475a31edd9990dd73f&chksm=ead030c4dda7b9d23ee48280fc1d7725434d4550b5fb810cef76f72c0bc8ad3bc451836260b3&scene=21#wechat_redirect
---
* 原文作者： IT码徒 
* 原文链接：[点击查看原文](https://mp.weixin.qq.com/s/H1XvlybQSnrRd7qb7mz-Pg)