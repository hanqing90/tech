-----
title:  开源! 基于lowcode行业的开源CMS系统,轻松帮助企业和个人搭建知识管理系统 
categories: 低代码
tags: ["低代码"] 
date: 2023-08-29
-----
![1ecde7402d20e063e773e07027ff7bb6.png][]

`lowcode-cms` 是我2年前在做**低代码平台**时开发的专门分享低代码资讯的社区, 集成了**内容发布**, **内容审核**, **媒体管理**, **用户体系**, **SSR渲染**, **跨多端适配**等功能模块的相对完整且轻量的CMS系统, 经历了2年多的内容历练和优化, 目前已完全可以承载各种行业的内容体系, 可以作为**个人网站**, **个人博客**, **中小企业知识分享平台**, **内容管理系统**.

为了更好的惠及更多的个人或者中小企业, 我将它正式在 `github`上开源. 文末会附上github地址, 欢迎各位参考体验.

### 1. 基于SSR技术+多端适配的内容端 ###

#### 1.1 内容分类 ####

![ac4a7b852ef9c10fdb795f165ba6c4be.png][]

内容详情:

![a7b737361556708b5a94e496bd5f8ca7.png][]

#### 1.2 全站搜索 ####

![454517b3fb6c78b0564ec19dea44b6ce.png][]

#### 1.3 视频专区 ####

![c3a71042e473d5bcb7bee3c0d8251d45.png][]

视频详情:![a9d727d27bfd3a1382b21665530d2324.png][]

#### 1.4 移动端自动适配 ####

![0ccf66289861201d99771f5f57f33db4.png][]![5044986b0b8f0d7186ec8dbe51e5655f.png][]

### 2. 管理端(内容+机构+用户管理, 内容审批, 内容编辑器) ###

#### 2.1 数据大盘 ####

![94ad11bcfdbfcc68c3f091e2910bd036.png][]

#### 2.2 网站管理 ####

![1158d4050cb8f95eaa4f5d93b5e8cb97.png][]

#### 2.3 机构管理 ####

![fcab0572b9bd22a4696c993a585ebed6.png][]

#### 2.4 内容管理 ####

![49d3fa735d50fe446c17265c3e95f93a.png][]

#### 2.5 内容审核 & 编辑 ####

![1fcf784866b7a45ec29c35e79fb21fa1.png][]

内容编辑所见即所得, 支持md和富文本编辑两种模式, 且实时预览.

#### 2.6 视频管理 ####

![c6e56674eed6f182c183d65326309908.png][]

### 3. 开箱即用的基于Koa2自研Nodejs服务端引擎 ###

![90023a2de83715ade0ce0fc6f4cdf05c.png][]

后台服务是采用我基于koa2自研的服务端框架, 感兴趣的可以参考学习一下. 后面我会在**趣谈前端**持续分享`lowcode-cms`的技术实现, 欢迎各位学习参考.

github地址: https://github.com/MrXujiang/lowcode-cms

lowcode-cms访问地址: www.lowcoder.cn

喜欢的话别忘了 分享、点赞、收藏 三连哦~。

![ad764194c977c8e513eb8c1f72d41620.png][]  


[从零搭建全栈可视化大屏制作平台V6.Dooring][V6.Dooring]  


[从零设计可视化大屏搭建引擎][Link 1]

[Dooring可视化搭建平台数据源设计剖析][Dooring]  


[可视化搭建的一些思考和实践][Link 2]  


[基于Koa + React + TS从零开发全栈文档编辑器(进阶实战][Koa _ React _ TS]

  


  


  


点个在看你最好看


[1ecde7402d20e063e773e07027ff7bb6.png]: https://hanqing90.gitee.io/image-hosting/2023/08/29/1ecde7402d20e063e773e07027ff7bb6.png
[ac4a7b852ef9c10fdb795f165ba6c4be.png]: https://hanqing90.gitee.io/image-hosting/2023/08/29/ac4a7b852ef9c10fdb795f165ba6c4be.png
[a7b737361556708b5a94e496bd5f8ca7.png]: https://hanqing90.gitee.io/image-hosting/2023/08/29/a7b737361556708b5a94e496bd5f8ca7.png
[454517b3fb6c78b0564ec19dea44b6ce.png]: https://hanqing90.gitee.io/image-hosting/2023/08/29/454517b3fb6c78b0564ec19dea44b6ce.png
[c3a71042e473d5bcb7bee3c0d8251d45.png]: https://hanqing90.gitee.io/image-hosting/2023/08/29/c3a71042e473d5bcb7bee3c0d8251d45.png
[a9d727d27bfd3a1382b21665530d2324.png]: https://hanqing90.gitee.io/image-hosting/2023/08/29/a9d727d27bfd3a1382b21665530d2324.png
[0ccf66289861201d99771f5f57f33db4.png]: https://hanqing90.gitee.io/image-hosting/2023/08/29/0ccf66289861201d99771f5f57f33db4.png
[5044986b0b8f0d7186ec8dbe51e5655f.png]: https://hanqing90.gitee.io/image-hosting/2023/08/29/5044986b0b8f0d7186ec8dbe51e5655f.png
[94ad11bcfdbfcc68c3f091e2910bd036.png]: https://hanqing90.gitee.io/image-hosting/2023/08/29/94ad11bcfdbfcc68c3f091e2910bd036.png
[1158d4050cb8f95eaa4f5d93b5e8cb97.png]: https://hanqing90.gitee.io/image-hosting/2023/08/29/1158d4050cb8f95eaa4f5d93b5e8cb97.png
[fcab0572b9bd22a4696c993a585ebed6.png]: https://hanqing90.gitee.io/image-hosting/2023/08/29/fcab0572b9bd22a4696c993a585ebed6.png
[49d3fa735d50fe446c17265c3e95f93a.png]: https://hanqing90.gitee.io/image-hosting/2023/08/29/49d3fa735d50fe446c17265c3e95f93a.png
[1fcf784866b7a45ec29c35e79fb21fa1.png]: https://hanqing90.gitee.io/image-hosting/2023/08/29/1fcf784866b7a45ec29c35e79fb21fa1.png
[c6e56674eed6f182c183d65326309908.png]: https://hanqing90.gitee.io/image-hosting/2023/08/29/c6e56674eed6f182c183d65326309908.png
[90023a2de83715ade0ce0fc6f4cdf05c.png]: https://hanqing90.gitee.io/image-hosting/2023/08/29/90023a2de83715ade0ce0fc6f4cdf05c.png
[ad764194c977c8e513eb8c1f72d41620.png]: https://hanqing90.gitee.io/image-hosting/2023/08/29/ad764194c977c8e513eb8c1f72d41620.png
[V6.Dooring]: http://mp.weixin.qq.com/s?__biz=MzU2Mzk1NzkwOA==&mid=2247489810&idx=1&sn=2663938569c4d361acae076bce9a9bf5&chksm=fc5300e9cb2489ff48db614f03093d71a81669bc6e0ccdf88b10af4a899b86ce59c6c95198b5&scene=21#wechat_redirect
[Link 1]: http://mp.weixin.qq.com/s?__biz=MzU2Mzk1NzkwOA==&mid=2247489467&idx=1&sn=be4b2a7f92f1c62e66432158212aee78&chksm=fc530e40cb248756193998c50b2e5e75de96dbde98170f871faa7e6ea043a50f87b7fc4468cf&scene=21#wechat_redirect
[Dooring]: http://mp.weixin.qq.com/s?__biz=MzU2Mzk1NzkwOA==&mid=2247487877&idx=2&sn=770ff16d69d3e7ac2bbcd78e97ab8f32&chksm=fc53087ecb2481685451a50e892fa889781788ca16a4ce689ec7f7fff1ae99c91ac8b82a160d&scene=21#wechat_redirect
[Link 2]: http://mp.weixin.qq.com/s?__biz=MzU2Mzk1NzkwOA==&mid=2247487950&idx=1&sn=e674a2f9379b9c9b8a149498a50c17f8&chksm=fc530835cb2481233acc7ac2c856b30c7698dd9d2aec1514b5c2fa4165bc2728f7b261c4938f&scene=21#wechat_redirect
[Koa _ React _ TS]: http://mp.weixin.qq.com/s?__biz=MzU2Mzk1NzkwOA==&mid=2247486910&idx=2&sn=7ce865dd8a8f6769439f0e8eebb72212&chksm=fc531445cb249d534a7d8a362ad40d26bc90f2d2e867385768ee19575e32826fcbe419fcbe0b&scene=21#wechat_redirect
---
* 原文作者： 趣谈前端 
* 原文链接：[点击查看原文](https://mp.weixin.qq.com/s/Lkg6a_zlyRTRa-HAKmXE_w)