-----
title:  一键生成Springboot & Vue项目
categories: 低代码
tags: ["MagicalCoder低代码"] 
date: 2023-08-01
-----
## 今天给大家推荐一款自己公司正在打磨的一款一键部署，一键生成全自动化的低代码生成器工具，可以实现前端可视化操作（拖拽形式+配置就可以生成前端页面），后端直接结合前端代码一键生成，数据库（含表字段）可一键生成（拖拽形式+配置），生成完成之后直接部署就可以了，生成的前端代码是Vue，后端代码是springboot。 ##

##     ##

目前还在不断的迭代当中，主要是想满足在校大学生的毕业设计的烦恼，一键生成完整的Springboot & Vue项目，毕设的功能基本上都够用了，功能是不是很强大了。

  


当然是拿出来学习的，所以也需要更多的喜欢这个项目的人加入我们来一起共同学习和完善这套系统。那么下面给大家介绍下功能吧！再提一句记住现在是免费的！

  


**功能如下：**

1.  数据仓库

新增数据库

新建表

组件拖拽

主键，输入框，密码框，下拉框，单选，多选，开关，滑块，日期，图片上传，评分，外键，颜色，计数器

1.  前端页面设计（暂时支持ElementUI）

组件拖拽

推荐表单，图片上传，复选框，评分，开关，远程下拉，远程标签，打印，链接按 钮，头像，图片，超链接，还包含一些日常表单组件，容器组件，导航组件，高级 组件

1.  Api列表

自定义设置后台接口，支持sql和json形式

  


**功能介绍：**

一、首先下载和安装

下载安装代码生成器工具链接地址见**文章底部**

1、下载好直接解压  


![e3567785402d531ba20ed42384604a53.png][]

2、直接进入文件夹里面记得文件夹要保持英文路径

![f74f5ae113bd4bdf1a1d8793d179061a.png][]

3、然后等待几分钟后会自动启动系统  


![97025af2722609c9cb2cb5737836b48e.png][]

  


会出现以下界面

![94ee738d24467c77d651b6cb1f19f7bc.png][]

  


二、注册  


注意：使用手机号注册

填写用户名和密码以及邀请码，没有邀请码是不能注册的  


![4ad5b5aa9717bfa9fb57baf2ed980380.png][]

然后根据方式获取邀请码填写即可

  


三、登录

  


![27d5a194e7d8fcce61dda31b21f21ead.png][]

  


四、数据仓库  


根据数据仓库设计数据库表字段，用于在页面设计时可以快速配置内置的CRUD代码实现。如下图：

1、新建仓库![d7345672f6492edf22895822f5807a69.png][]

2、按钮功能介绍![47ced95eace003fd9cb027aee3f763af.png][]

3、数据库字段设计及生成代码

![a22cb993a4e1b5bfc451789d29e2f026.png][]

![24a2b6e2615059c620c295a6f66f16cd.png][]

![391bb7741b6ff9e1a8235e41ca0fdfc5.png][]

![8cd688a78adf47f9a8b41b8cf41bf839.png][]

数据仓库这里就成功生成了。

  


1.  页面设计
2.  进入页面设计新建一个页面

![f60478f3b67782625a040b499ea0226b.png][]

![99939048da088516f7ef0604435d3c20.png][]

  


1.  新增完成之后可以自定义修改页面名称

![3870f71d7f50a67c71855474f2a768c1.png][]

3、点击组件按钮查看所有组件

官方组件

推荐表单，图片上传，复选框，评分，开关，远程下拉，远程标签，打印，链接按 钮，头像，图片，超链接，还包含一些日常表单组件，容器组件，导航组件，高级组件

![06740e917c06b77bba20cbecffb09f65.png][]

如果以上组件不满足需求可以在组件市场进行下载

![337001e297b6460a767b121004a7ae54.png][]

也可以自行开发组件使用。

  


父子表组件展示：

![110d1eaf935054c875c8e6101b5a404a.png][]

  


拖拉完成之后，数据仓库中的数据字段就直接在里面呈现，接着点击保存页面，最后预览页面，就可以进行CRUD的操作了。

预览页面进行添加

![e69807df5b05c29e1658ce04650d2be6.png][]

进入添加操作

![f986454a73b08cea561c85b182cbf396.png][]

![a20d8e606f80e8730a4cca35a0559c88.png][]

以上还可以直接打印、导出等功能。

  


图表展示：

![b87373e8da38a61975a66b46b801f705.png][]

一共可以实现几十种统计的图表。

  


代码生成器的大致使用就介绍完了，下面来介绍一下前后端的代码生成到哪里了。

打开你安装的代码生成器目录，**找到test文件夹就可以找到Java代码了**。

![682480057e1b543b2b9f4b98cca0420e.png][]

前端代码可以直接复制本地保存即可，也可以直接下载。

![0b057dc7334488b4c7dd7b396a466327.png][]

最后您还可以把低代码平台以maven的方式集成到自己的springboot项目中，参考project/test项目配置。

  


**下载地址：**https://wwa.lanzoui.com/b0cwp2nte


[e3567785402d531ba20ed42384604a53.png]: https://hanqing90.gitee.io/image-hosting/2023/08/01/e3567785402d531ba20ed42384604a53.png
[f74f5ae113bd4bdf1a1d8793d179061a.png]: https://hanqing90.gitee.io/image-hosting/2023/08/01/f74f5ae113bd4bdf1a1d8793d179061a.png
[97025af2722609c9cb2cb5737836b48e.png]: https://hanqing90.gitee.io/image-hosting/2023/08/01/97025af2722609c9cb2cb5737836b48e.png
[94ee738d24467c77d651b6cb1f19f7bc.png]: https://hanqing90.gitee.io/image-hosting/2023/08/01/94ee738d24467c77d651b6cb1f19f7bc.png
[4ad5b5aa9717bfa9fb57baf2ed980380.png]: https://hanqing90.gitee.io/image-hosting/2023/08/01/4ad5b5aa9717bfa9fb57baf2ed980380.png
[27d5a194e7d8fcce61dda31b21f21ead.png]: https://hanqing90.gitee.io/image-hosting/2023/08/01/27d5a194e7d8fcce61dda31b21f21ead.png
[d7345672f6492edf22895822f5807a69.png]: https://hanqing90.gitee.io/image-hosting/2023/08/01/d7345672f6492edf22895822f5807a69.png
[47ced95eace003fd9cb027aee3f763af.png]: https://hanqing90.gitee.io/image-hosting/2023/08/01/47ced95eace003fd9cb027aee3f763af.png
[a22cb993a4e1b5bfc451789d29e2f026.png]: https://hanqing90.gitee.io/image-hosting/2023/08/01/a22cb993a4e1b5bfc451789d29e2f026.png
[24a2b6e2615059c620c295a6f66f16cd.png]: https://hanqing90.gitee.io/image-hosting/2023/08/01/24a2b6e2615059c620c295a6f66f16cd.png
[391bb7741b6ff9e1a8235e41ca0fdfc5.png]: https://hanqing90.gitee.io/image-hosting/2023/08/01/391bb7741b6ff9e1a8235e41ca0fdfc5.png
[8cd688a78adf47f9a8b41b8cf41bf839.png]: https://hanqing90.gitee.io/image-hosting/2023/08/01/8cd688a78adf47f9a8b41b8cf41bf839.png
[f60478f3b67782625a040b499ea0226b.png]: https://hanqing90.gitee.io/image-hosting/2023/08/01/f60478f3b67782625a040b499ea0226b.png
[99939048da088516f7ef0604435d3c20.png]: https://hanqing90.gitee.io/image-hosting/2023/08/01/99939048da088516f7ef0604435d3c20.png
[3870f71d7f50a67c71855474f2a768c1.png]: https://hanqing90.gitee.io/image-hosting/2023/08/01/3870f71d7f50a67c71855474f2a768c1.png
[06740e917c06b77bba20cbecffb09f65.png]: https://hanqing90.gitee.io/image-hosting/2023/08/01/06740e917c06b77bba20cbecffb09f65.png
[337001e297b6460a767b121004a7ae54.png]: https://hanqing90.gitee.io/image-hosting/2023/08/01/337001e297b6460a767b121004a7ae54.png
[110d1eaf935054c875c8e6101b5a404a.png]: https://hanqing90.gitee.io/image-hosting/2023/08/01/110d1eaf935054c875c8e6101b5a404a.png
[e69807df5b05c29e1658ce04650d2be6.png]: https://hanqing90.gitee.io/image-hosting/2023/08/01/e69807df5b05c29e1658ce04650d2be6.png
[f986454a73b08cea561c85b182cbf396.png]: https://hanqing90.gitee.io/image-hosting/2023/08/01/f986454a73b08cea561c85b182cbf396.png
[a20d8e606f80e8730a4cca35a0559c88.png]: https://hanqing90.gitee.io/image-hosting/2023/08/01/a20d8e606f80e8730a4cca35a0559c88.png
[b87373e8da38a61975a66b46b801f705.png]: https://hanqing90.gitee.io/image-hosting/2023/08/01/b87373e8da38a61975a66b46b801f705.png
[682480057e1b543b2b9f4b98cca0420e.png]: https://hanqing90.gitee.io/image-hosting/2023/08/01/682480057e1b543b2b9f4b98cca0420e.png
[0b057dc7334488b4c7dd7b396a466327.png]: https://hanqing90.gitee.io/image-hosting/2023/08/01/0b057dc7334488b4c7dd7b396a466327.png
---
* 原文作者： java代码 
* 原文链接：[点击查看原文](https://mp.weixin.qq.com/s/zZYhlTO4Gh248ZX_hoh0-Q)